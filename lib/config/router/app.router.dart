import 'package:go_router/go_router.dart';
import 'package:notification_push/presentation/screens/screens.module.dart';

final appRouter = GoRouter(
  initialLocation: '/',
  routes: [
    GoRoute(
      name: HomeScreen.name,
      path: '/',
      builder: (context, state) {
        return const HomeScreen();
      },
      routes: [
        GoRoute(
          name: DetailNotificationScreen.name,
          path: 'notification/detail/:messageId',
          builder: (context, state) {
            return DetailNotificationScreen(
              messageId: state.pathParameters['messageId'] ?? '',
            );
          },
        )
      ],
    ),
  ],
);
