import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notification_push/config/local_notifications/local_notifications.config.dart';
import 'package:notification_push/presentation/blocs/notifications/notifications_bloc.dart';

import 'config/router/app.router.dart';
import 'config/themes/app.theme.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  FirebaseMessaging.onBackgroundMessage(firebaseMessagingBackgroundHandler);
  await NotificationsBloc.initFirebaseNotifications();
  await LocalNotifications.initLocalNotifications();

  runApp(
    MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (_) => NotificationsBloc(
            requestPermissionLocalNotification: LocalNotifications.requestPermissionLocalNotification,
            showLocalNotifications: LocalNotifications.showLocalNotifications,
          ),
        ),
      ],
      child: const MainApp(),
    ),
  );
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      debugShowCheckedModeBanner: false,
      theme: AppTheme(
        selectColor: 2,
        mode: Brightness.light,
      ).theme(),
      routerConfig: appRouter,
      builder: (context, child) => HandlerNotificationInteractions(
        child: child!,
      ),
    );
  }
}

class HandlerNotificationInteractions extends StatefulWidget {
  final Widget child;

  const HandlerNotificationInteractions({
    super.key,
    required this.child,
  });

  @override
  State<HandlerNotificationInteractions> createState() => _HandlerNotificationInteractionsState();
}

class _HandlerNotificationInteractionsState extends State<HandlerNotificationInteractions> {
  Future<void> setupInteractedMessage() async {
    // Get any messages which caused the application to open from
    // a terminated state.
    RemoteMessage? initialMessage = await FirebaseMessaging.instance.getInitialMessage();

    // If the message also contains a data property with a "type" of "chat",
    // navigate to a chat screen
    if (initialMessage != null) {
      _handleMessage(initialMessage);
    }

    // Also handle any interaction when the app is in the background via a
    // Stream listener
    FirebaseMessaging.onMessageOpenedApp.listen(_handleMessage);
  }

  void _handleMessage(RemoteMessage message) {
    context.read<NotificationsBloc>().handlerRemoteMessage(message);
    final messageId = context.read<NotificationsBloc>().formatMessageId(message.messageId);
    appRouter.push("/notification/detail/$messageId");
  }

  @override
  void initState() {
    super.initState();

    // Run code required to handle interacted messages in an async function
    // as initState() must not be async
    setupInteractedMessage();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
