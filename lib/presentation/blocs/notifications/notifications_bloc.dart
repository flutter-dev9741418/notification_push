import 'dart:io';

import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:notification_push/domain/entities/push_message.entity.dart';
import 'package:notification_push/firebase_options.dart';

part 'notifications_event.dart';
part 'notifications_state.dart';

Future<void> firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  await Firebase.initializeApp();
  debugPrint(
      "notifications_bloc ~ firebaseMessagingBackgroundHandler ~ message.messageId ~ ${message.messageId}");
}

class NotificationsBloc extends Bloc<NotificationsEvent, NotificationsState> {
  FirebaseMessaging messaging = FirebaseMessaging.instance;

  final Future<void> Function()? requestPermissionLocalNotification;
  final void Function({
    required int id,
    String? title,
    String? body,
    String? data,
  })? showLocalNotifications;

  NotificationsBloc({
    this.requestPermissionLocalNotification,
    this.showLocalNotifications,
  }) : super(const NotificationsState()) {
    on<NotificationsStatusChanged>(_notificationsStatusChanged);
    on<NotificationsTokenChanged>(_notificationsTokenChanged);
    on<NotificationRecived>(_addNotifications);
    on<NotificationIdIncrement>(_incrementId);

    _initialStatusCheck();
    _onForegroundMessage();
  }

  static Future<void> initFirebaseNotifications() async {
    await Firebase.initializeApp(
      options: DefaultFirebaseOptions.currentPlatform,
    );
  }

  void _initialStatusCheck() async {
    final settings = await messaging.getNotificationSettings();
    add(NotificationsStatusChanged(settings.authorizationStatus));
  }

  void _incrementId(
    NotificationIdIncrement event,
    Emitter<NotificationsState> emit,
  ) {
    emit(
      state.copyWith(countId: state.countId + 1),
    );
  }

  void _notificationsStatusChanged(
    NotificationsStatusChanged event,
    Emitter<NotificationsState> emit,
  ) {
    emit(
      state.copyWith(
        status: event.status,
      ),
    );

    _getToken();
  }

  void _notificationsTokenChanged(
    NotificationsTokenChanged event,
    Emitter<NotificationsState> emit,
  ) {
    emit(
      state.copyWith(
        token: event.token,
      ),
    );
  }

  void _addNotifications(
    NotificationRecived event,
    Emitter<NotificationsState> emit,
  ) {
    emit(
      state.copyWith(
        notifications: [event.notification, ...state.notifications],
      ),
    );

    debugPrint("NotificationsBloc ~ _addNotifications ~ state.notifications ~ ${state.notifications.length}");
  }

  void requestPermissions() async {
    NotificationSettings settings = await messaging.requestPermission(
      alert: true,
      announcement: false,
      badge: true,
      carPlay: false,
      criticalAlert: true,
      provisional: false,
      sound: true,
    );

    if (requestPermissionLocalNotification != null) {
      await requestPermissionLocalNotification!();
    }

    add(NotificationsStatusChanged(settings.authorizationStatus));
  }

  void _getToken() async {
    final settings = await messaging.getNotificationSettings();
    if (settings.authorizationStatus != AuthorizationStatus.authorized) return;

    final token = await messaging.getToken();
    add(NotificationsTokenChanged(token ?? ""));
    debugPrint("NotificationsBloc ~ _getToken ~ token ~ $token");
  }

  void handlerRemoteMessage(RemoteMessage message) {
    if (message.notification == null) return;

    add(NotificationIdIncrement());

    final PushMessageEntity noti = PushMessageEntity(
      messageId: formatMessageId(message.messageId),
      title: message.notification!.title ?? '',
      body: message.notification!.body ?? '',
      sentDate: message.sentTime ?? DateTime.now(),
      data: message.data,
      imageUrl: Platform.isAndroid
          ? message.notification!.android?.imageUrl
          : message.notification!.apple?.imageUrl,
    );

    if (showLocalNotifications != null) {
      showLocalNotifications!(
        id: state.countId,
        title: message.notification!.title ?? '',
        body: message.notification!.body ?? '',
        data: noti.messageId,
      );
    }

    debugPrint("$noti");
    add(NotificationRecived(noti));
  }

  void _onForegroundMessage() {
    FirebaseMessaging.onMessage.listen(handlerRemoteMessage);
  }

  String formatMessageId(String? messageId) {
    return messageId?.replaceAll(":", "").replaceAll("%", "") ?? '';
  }

  PushMessageEntity? getMessageById(String messageId) {
    final exits = state.notifications.any((item) => item.messageId == messageId);

    if (!exits) return null;

    return state.notifications.firstWhere((item) => item.messageId == messageId);
  }
}
