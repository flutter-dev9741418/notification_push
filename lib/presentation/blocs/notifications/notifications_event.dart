part of 'notifications_bloc.dart';

abstract class NotificationsEvent {
  const NotificationsEvent();
}

class NotificationsStatusChanged extends NotificationsEvent {
  final AuthorizationStatus status;

  const NotificationsStatusChanged(this.status);
}

class NotificationsTokenChanged extends NotificationsEvent {
  final String token;

  const NotificationsTokenChanged(this.token);
}

class NotificationRecived extends NotificationsEvent {
  final PushMessageEntity notification;

  const NotificationRecived(this.notification);
}

class NotificationIdIncrement extends NotificationsEvent {}
