part of 'notifications_bloc.dart';

class NotificationsState extends Equatable {
  final int countId;
  final AuthorizationStatus status;
  final String token;
  final List<PushMessageEntity> notifications;

  const NotificationsState({
    this.countId = 0,
    this.status = AuthorizationStatus.notDetermined,
    this.token = "",
    this.notifications = const [],
  });

  copyWith({
    int? countId,
    AuthorizationStatus? status,
    List<PushMessageEntity>? notifications,
    String? token,
  }) {
    return NotificationsState(
      countId: countId ?? this.countId,
      notifications: notifications ?? this.notifications,
      status: status ?? this.status,
      token: token ?? this.token,
    );
  }

  @override
  List<Object> get props => [countId, status, notifications, token];
}
