import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:notification_push/domain/entities/push_message.entity.dart';
import 'package:notification_push/presentation/blocs/notifications/notifications_bloc.dart';

class DetailNotificationScreen extends StatelessWidget {
  static const name = "DetailNotificationScreen";

  final String messageId;

  const DetailNotificationScreen({
    super.key,
    required this.messageId,
  });

  @override
  Widget build(BuildContext context) {
    final notification =
        context.read<NotificationsBloc>().getMessageById(messageId);

    return Scaffold(
      appBar: AppBar(
        title: const Text("Detalle"),
      ),
      body: notification != null
          ? _DetailNotificationView(notification: notification)
          : const Center(
              child: Text("La notificación no existe."),
            ),
    );
  }
}

class _DetailNotificationView extends StatelessWidget {
  final PushMessageEntity notification;

  const _DetailNotificationView({
    required this.notification,
  });

  @override
  Widget build(BuildContext context) {
    final textTheme = Theme.of(context).textTheme;

    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: 10,
        vertical: 20,
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          if (notification.imageUrl != null)
            Image.network(notification.imageUrl!, width: double.infinity),
          if (notification.imageUrl != null)
            const SizedBox(
              height: 30,
            ),
          Text(
            notification.title,
            style: textTheme.titleMedium,
            textAlign: TextAlign.left,
          ),
          Text(
            notification.body,
            style: textTheme.bodyMedium,
            textAlign: TextAlign.left,
          ),
          const Divider(),
          Text(notification.data.toString()),
        ],
      ),
    );
  }
}
