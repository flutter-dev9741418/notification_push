import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:go_router/go_router.dart';
import 'package:notification_push/presentation/blocs/notifications/notifications_bloc.dart';

class HomeScreen extends StatelessWidget {
  static const name = "HomeScreen";

  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Notificaciones Push"),
        actions: [
          IconButton(
            onPressed: context.select(
              (NotificationsBloc bloc) => bloc.state.status ==
                      AuthorizationStatus.authorized
                  ? null
                  : () {
                      context.read<NotificationsBloc>().requestPermissions();
                    },
            ),
            icon: context.select(
              (NotificationsBloc bloc) =>
                  bloc.state.status != AuthorizationStatus.authorized
                      ? const Icon(Icons.notifications_off)
                      : const Icon(Icons.notifications),
            ),
          ),
        ],
      ),
      body: const _HomeView(),
    );
  }
}

class _HomeView extends StatelessWidget {
  const _HomeView();

  @override
  Widget build(BuildContext context) {
    final notifications =
        context.watch<NotificationsBloc>().state.notifications;
    return ListView.builder(
      itemCount: notifications.length,
      itemBuilder: (context, index) {
        final notification = notifications[index];

        return ListTile(
          onTap: () =>
              context.push("/notification/detail/${notification.messageId}"),
          title: Text(notification.title),
          subtitle: Text(notification.body),
          leading: notification.imageUrl != null
              ? Image.network(notification.imageUrl!)
              : null,
        );
      },
    );
  }
}
